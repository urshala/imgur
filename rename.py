import os

current_location = os.getcwd()

for index, items in enumerate(os.listdir()):
	if items.endswith('.jpg'):
		os.rename(os.path.join(current_location,items), os.path.join(current_location, 'photo%s.jpg' %str(index)))
