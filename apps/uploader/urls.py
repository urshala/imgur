from django.conf.urls import url
from . import views 

urlpatterns = [
	url(r'^$', views.index_view, name='index_page'),
	url(r'^upload/$', views.upload_view, name='upload_page'),
	url(r'^account/registration/$', views.user_registration, name='register'),
	url(r'^account/login/$', views.user_login, name='login_page'),
	url(r'^account/logout/$', views.user_logout, name='logout_page'),
	url(r'^like/$', views.increase_like, name='like_counter'),
]