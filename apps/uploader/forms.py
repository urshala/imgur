from django import forms
from .models import UploadedImages, UserProfile
from django.contrib.auth.models import User


class ImageForm(forms.ModelForm):
	class Meta:
		model = UploadedImages
		fields = ('title','image')


class ProfileForm(forms.ModelForm):
	class Meta:
		model = UserProfile
		fields = ('profile_picture',)


class UserForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput())
	class Meta:
		model = User
		fields = ('username', 'email', 'password')
		help_texts = {
		'email': None,
		'username': None
		}