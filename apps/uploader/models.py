from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.

class UploadedImages(models.Model):
	title = models.CharField(max_length=120)
	image = models.ImageField(upload_to = 'flickr_image/')
	date = models.DateTimeField(default=timezone.now())

	def __str__(self):
		return self.title

class UserProfile(models.Model):
	user = models.ForeignKey(User)
	profile_picture = models.ImageField(upload_to='profile_image/', blank=True)




class UserLikes(models.Model):
	value = models.BooleanField(default=False)
	img = models.ForeignKey(UploadedImages, related_name="like", on_delete=models.CASCADE, null=True, blank=True)
	user = models.ForeignKey(UserProfile, related_name="like", on_delete=models.CASCADE, null=True, blank=True)



class UserDisLikes(models.Model):
	value = models.BooleanField(default=False)
	img = models.ForeignKey(UploadedImages, related_name="dislike", on_delete=models.CASCADE, null=True, blank=True)
	user = models.ForeignKey(UserProfile, related_name="dislike", on_delete=models.CASCADE, null=True, blank=True)
