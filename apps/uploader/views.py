from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate,logout, login
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.
from .models import UploadedImages, UserLikes
from .forms import ImageForm, ProfileForm, UserForm


def index_view(request):
	images_list = UploadedImages.objects.all()
	# print images_list
	print  UserLikes.objects.filter(value=True)
	for image in images_list:
		print image
		print UserLikes.objects.filter(value=True, img=image)
	# page = request.GET.get('page')
	# paginator = Paginator(images_list,8)
	# try:
	# 	images = paginator.page(page)
	# except PageNotAnInteger:
	# 	images = paginator.page(1)
	# except EmptyPage:
	# 	images = paginator.page(paginator.num_pages)
	return render(request,'uploader/index.html', {'images': images_list})



@login_required()
def upload_view(request):
	if request.method == 'POST':
		form = ImageForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse('index_page'))
		else:
			print (form.errors)
	else:
		form = ImageForm()
	return render(request, 'uploader/upload.html', {'form':form})


def user_registration(request):
	if request.method == 'POST':
		user_form = UserForm(request.POST)
		profile_form = ProfileForm(request.POST, request.FILES)
		if user_form.is_valid() and profile_form.is_valid():
			user = user_form.save()
			user.set_password(user.password)
			user.save()


			p_form = profile_form.save(commit=False)
			if p_form.profile_picture:
				print ('There is profile pic')
			else:
				print ('No profie pic')
			p_form.user = user
			#p_form.profile_picture = request.FILES['profile_picture']
			p_form.save()

			#log-in the user automatically
			username = request.POST['username']
			password = request.POST['password']
			print (username, password)
			user = authenticate(username = username, password=password)
			if user:
				login(request,user)
				return HttpResponseRedirect(reverse('index_page'))
			else:
				return HttpResponse('Oops there is some error in your registration. Please check it again.')

		else:
			print(profile_form.errors)

	else:
		profile_form = ProfileForm()
		user_form = UserForm()
	return render(request, 'uploader/registration.html', {'profile_form':profile_form, 'user_form':user_form})


def user_login(request):
	next_page = request.GET.get('next')
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username=username, password=password)
		if user:
			login(request,user)
			if next_page:
				return redirect(next_page)
			return HttpResponseRedirect(reverse('index_page'))
		else:
			return HttpResponse('The username or password is incorrect.')
	else:
		return render(request,'uploader/login.html', {'next_page':next_page})

def user_logout(request):
	logout(request)
	return HttpResponseRedirect(reverse('index_page'))



def increase_like(request):
	if not request.user.is_authenticated():
		return HttpResponse('')
	else:
		img_id = request.GET['img_id']
		if img_id:
			selected_image = UploadedImages.objects.get(id=int(img_id))
			# if selected_image.like != 0;
			like = 0
			like = selected_image.like + 1
			selected_image.like = like
			selected_image.save()
		return HttpResponse(like)
	